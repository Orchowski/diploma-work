# Praca dyplomowa - Aleksander Orchowski
## Kodowanie plików
Kodowanie plików .tex to - windows-1250/cp1250/ibm-5346

Aby pliki były odczytywalne w trakcie review zastosowano skrypt beforePush.sh który zmienia kodowanie na UTF-8.
Przed rozpoczęciem zmian trzeba wykonać skrypt afterPush.sh, a po zakończonej pracy wykonać beforePush.sh

Jest to uciążliwe ale template z UZ jest tak wykonany i nie widzę innej możliwości bez przerabiania template'u.

główny plik ze stroną tyułową może być nieczytelny na stronie.

## Rysunki
W folderze "Rysunki" znajdują się wszelkie ilustracje. Wewnątrz znajdują się podfoldery podzielone według rozdziałów.

## Bibliografia
Format bibliografii zostanie jeszcze ustalony. Znajduje się w folderze o nazwie "bibliografia".

## USTALENIA
Wszelkie ustalenia ze spotkań z promotorem znajdują się w folderze "Ustalenia". Dowolny format + .pdf w celu łatwego podglądu na repozytorium.

## Inne
W głównym katalogu znajdują się też karta pracy, oraz szablon .docx.